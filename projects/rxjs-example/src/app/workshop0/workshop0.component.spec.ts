import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Workshop0Component } from './workshop0.component';

describe('Workshop0Component', () => {
  let component: Workshop0Component;
  let fixture: ComponentFixture<Workshop0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Workshop0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Workshop0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
