import { Component, OnInit } from '@angular/core';
import { Observable, of, asyncScheduler } from 'rxjs';

// const number$ = Observable.of(1, 2, 3, 4, 5);
const number = [1, 2, 3, 4, 5];
@Component({
  selector: 'app-workshop0',
  templateUrl: './workshop0.component.html',
  styleUrls: ['./workshop0.component.scss']
})
export class Workshop0Component implements OnInit {

  constructor() {
    const p = number.map(n => n * 2).filter(n => n > 7);
    // log evrery time
    console.log('promist');
    console.log(p);
  }

  ngOnInit() {
    //const p = new Promise((res, rej) => {
    //   res(5);
    // });
    // p.then((r) => console.log(r));

    // const o = new Observable((s) => {
    //   console.log('Hey');

    //   s.next(224);
    // });
    // must subscribe
    // o.subscribe();

    // total 50

    // I don't care
    // sync
    /// Execution Context of Observable.



  }

  func1() {
    of('observable').subscribe(console.log); // sync


    Promise.resolve('promiste (macro)').then((r) => {
      console.log(r);
    }); // async

    setTimeout(() =>
      console.log('timeout (micro)')
      , 0); // async

    console.log('sync'); // sync
  }
  func2() {
 //   of('observable').pipe(ObserveOn(asyncScheduler))

   //   .subscribe(console.log); // sync

  }

}
