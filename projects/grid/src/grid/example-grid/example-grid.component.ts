import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-example',
  templateUrl: './example-grid.component.html',
  styleUrls: ['./example-grid.component.scss']
})
export class ExampleGridComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
