import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamplePokemonComponent } from './example-pokemon.component';

describe('ExamplePokemonComponent', () => {
  let component: ExamplePokemonComponent;
  let fixture: ComponentFixture<ExamplePokemonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamplePokemonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamplePokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
