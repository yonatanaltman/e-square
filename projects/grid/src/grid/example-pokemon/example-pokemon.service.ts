import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pokemon } from './example-pokemon.component';

@Injectable()
export class ExamplePokemonService {
  pokemonsArray: Pokemon[] = [
    { name: 'Chikorita', img: 'https://cdn.bulbagarden.net/upload/b/bf/152Chikorita.png' },
    { name: 'Smoochum', img: 'https://cdn.bulbagarden.net/upload/0/0e/238Smoochum.png' },
    { name: 'bellsprout', img: 'http://ja.pokemongopokedex.site/static/img/pkk/bellsprout.png' },
    { name: 'Pichu', img: 'http://cdn.bulbagarden.net/upload/b/b9/172Pichu.png' },
    { name: 'Marill', img: 'http://cdn.bulbagarden.net/upload/4/42/183Marill.png' },
    { name: 'Suicune', img: 'http://cdn.bulbagarden.net/upload/d/da/245Suicune.png' },
    { name: 'Cyndaquil', img: 'http://cdn.bulbagarden.net/upload/9/9b/155Cyndaquil.png' },
    { name: 'Wobbuffet', img: 'http://cdn.bulbagarden.net/upload/1/17/202Wobbuffet.png' },
    { name: 'Slugma', img: 'https://cdn.bulbagarden.net/upload/6/68/218Slugma.png' },
    { name: 'Celebi', img: 'https://cdn.bulbagarden.net/upload/e/e7/251Celebi.png' },
    { name: 'Phanpy', img: 'https://cdn.bulbagarden.net/upload/d/d3/231Phanpy.png' }
  ];
  constructor() { }
  public getPokemons(): Observable<Pokemon[]> {
    const pokemons = new Observable<Pokemon[]>(
      (observer) => {
        observer.next(this.pokemonsArray);
      });
    return pokemons;
  }
}
