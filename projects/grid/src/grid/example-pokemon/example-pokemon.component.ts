import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ExamplePokemonService } from './example-pokemon.service';

export interface Pokemon {
  name: string;
  img: string;
}
@Component({
  selector: 'app-example-pokemon',
  templateUrl: './example-pokemon.component.html',
  styleUrls: ['./example-pokemon.component.scss'],
  providers: [ExamplePokemonService]
})
export class ExamplePokemonComponent implements OnInit {
  pokemons: Observable<Pokemon[]>;
  constructor(private service: ExamplePokemonService) { }

  ngOnInit() {
    this.pokemons = this.service.getPokemons();
  }

}


