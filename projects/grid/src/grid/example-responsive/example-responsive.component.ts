import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-responsive',
  templateUrl: './example-responsive.component.html',
  styleUrls: ['./example-responsive.component.scss']
})
export class ExampleResponsiveComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
