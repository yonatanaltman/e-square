import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-blocks',
  templateUrl: './example-blocks.component.html',
  styleUrls: ['./example-blocks.component.scss']
})
export class ExampleBlocksComponent implements OnInit {
  numbers: string[];
  constructor() { }

  ngOnInit() {
    const arr = [];
    for (let i = 1; i < 20; i++) {
      arr.push('' + i);

    }
    this.numbers = arr;
  }

}
