import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-sandbox',
  templateUrl: './example-sandbox.component.html',
  styleUrls: ['./example-sandbox.component.scss']
})
export class ExampleSandboxComponent implements OnInit {
  labels: string[] = [];
  constructor() {
    for (let i = 1; i < 5; i++) {
      this.labels.push('' + i);
    }
  }

  ngOnInit() {
  }

}
