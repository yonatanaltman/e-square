import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-example-full',
  templateUrl: './example-full.component.html',
  styleUrls: ['./example-full.component.scss']
})
export class ExampleFullComponent implements OnInit {

  static label = 'So Much Room for Activities!';

  title = 'Full-Viewport Application';
  showNav = true;

  imageItems: Observable<string[]>;

  constructor() {

  }
  ngOnInit(): void {

  }
}
