import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleFullComponent } from './example-full.component';

describe('ExampleFullComponent', () => {
  let component: ExampleFullComponent;
  let fixture: ComponentFixture<ExampleFullComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleFullComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
