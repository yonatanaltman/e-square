import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExampleGridComponent } from './example-grid/example-grid.component';
import { RouterModule } from '@angular/router';
import { GridComponent } from './grid/grid.component';
import { ExampleBlocksComponent } from './example-blocks/example-blocks.component';
import { ExampleDocComponent } from './example-doc/example-doc.component';
import { ExampleAreaComponent } from './example-area/example-area.component';
import { ExampleResponsiveComponent } from './example-responsive/example-responsive.component';
import { ExamplePokemonComponent } from './example-pokemon/example-pokemon.component';
import { ExampleFullComponent } from './example-full/example-full.component';
import { ExampleSandboxComponent } from './example-sandbox/example-sandbox.component';

const EXPORT = [
    GridComponent,
    ExampleGridComponent,
    ExampleBlocksComponent,
    ExampleDocComponent,
    ExampleAreaComponent,
    ExampleResponsiveComponent,
    ExamplePokemonComponent,
    ExampleFullComponent,
    ExampleSandboxComponent];

@NgModule({
    declarations: EXPORT,
    imports: [CommonModule,
        RouterModule.forRoot([
            {
                path: 'grid', component: GridComponent, children: [
                    { path: 'example', component: ExampleGridComponent },
                    { path: 'blocks', component: ExampleBlocksComponent },
                    { path: 'document', component: ExampleDocComponent },
                    { path: 'area', component: ExampleAreaComponent },
                    { path: 'responsive', component: ExampleResponsiveComponent },
                    { path: 'pokemon', component: ExamplePokemonComponent },
                    { path: 'sandbox', component: ExampleSandboxComponent },
                    { path: 'full', component: ExampleFullComponent },

                ]
            },
           // { path: '**', redirectTo: 'grid'}
        ])],
    exports: EXPORT,
    providers: [],
})
export class GridModule { }
