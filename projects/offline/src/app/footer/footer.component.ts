import { Component, OnInit } from '@angular/core';
import { OrderService } from '../service/order.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public get nextOrderIndex(): number {
    return this.orderService.nextOrderIndex;
  }
  public get count$(): Observable<number> {
    return this.orderService.List$.pipe(map(list => list.length));
  }
  title = {
    next: 'next order list',
    count: 'number of orders',
  };
  constructor(private orderService: OrderService) { }

  ngOnInit() {
  }

}
