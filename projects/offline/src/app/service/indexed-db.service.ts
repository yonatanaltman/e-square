import { Injectable } from '@angular/core';
import { openDB, deleteDB, wrap, unwrap, IDBPDatabase } from 'idb';
import { Observable, Subject, ReplaySubject } from 'rxjs';
const takeawayDBName = 'takeaway';
const ordersStoreName = 'OrdersStore';
const takeawayDBVersion = 1;
export interface IOrder {
  orderid: number;
  name: string;
}
export interface IError {
  time: Date;
  code: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class IndexedDbService {
  // tslint:disable-next-line: variable-name
  private dataChange$ = new ReplaySubject<IOrder[]>(1);
  private onError$ = new ReplaySubject<IError>(1);
  private onComplete$ = new ReplaySubject<any>(1);

  // tslint:disable-next-line: variable-name
  private _dbPromise: Promise<IDBPDatabase<unknown>>;

  constructor() {
    this.connectToIDB();
  }
  public init(stores: string[]) {
    stores.forEach(s => {

      this.getAllData(s).then(items => {
        this.dataChange$.next(items);
      });
    });
  }
  connectToIDB() {
    this._dbPromise = openDB(takeawayDBName, takeawayDBVersion, {
      upgrade(db, oldVersion, newVersion, transaction /** */) {
        console.log(`Upgrade to --V${newVersion}-- from --V${oldVersion}--`);
        // init my stores
        if (!db.objectStoreNames.contains(ordersStoreName)) {
          const store = db.createObjectStore(ordersStoreName, { keyPath: 'orderid' });
          store.createIndex('orderid', 'orderid', { unique: true });

        }

        // An enhanced transaction for this upgrade. This is useful if you need to get data from other stores as part of a migration.
        const tran = transaction;
      },
      blocked() {
        // Called if there are older versions of the database open on the origin, so this version cannot open.
      },
      blocking() {
        // Called if this connection is blocking a future version of the database from opening.
      }
    });

  }

  /**
   * deleting a data base from browser's indexedDB
   * @param dbName the name of the data base to delete
   */
  deleteDBbyName(dbName: string): Promise<void> {
    return deleteDB(dbName);
  }

  public addItem(targetStoreName: string, value: any) {
    this._dbPromise.then((db: IDBPDatabase<unknown>) => {
      const tx = db.transaction(targetStoreName, 'readwrite');
      try {
        const objectStore = tx.objectStore(targetStoreName);
        objectStore.put({ orderid: value.orderid, name: value.name });
        this.getAllData(targetStoreName).then((items: IOrder[]) => {
          this.dataChange$.next(items);
        });
        return tx.oncomplete;
      } catch (error) {
        console.log('addItems', error);
        this.onError$.next({ code: '1', message: error, time: new Date() });
        return tx.onerror;
      }
    }).catch(error => {
      this.onError$.next(error);
    }).finally(() => {
      this.onComplete$.next('done');
    });
  }
  public deleteItem(targetStoreName: string, value: any) {
    this._dbPromise.then((db: IDBPDatabase<unknown>) => {
      const tx = db.transaction(targetStoreName, 'readwrite');
      try {
        const objectStore = tx.objectStore(targetStoreName);
        objectStore.delete(value.orderid);
        this.getAllData(targetStoreName).then((items: IOrder[]) => {
          this.dataChange$.next(items);
        });
        return tx.oncomplete;
      } catch (error) {
        console.log('addItems', error);
        this.onError$.next({ code: '1', message: error, time: new Date() });
        return tx.onerror;
      }
    }).catch(error => {
      this.onError$.next(error);
    }).finally(() => {
      this.onComplete$.next('done');
    });
  }
  onerror(error) {
    this.onError$.next(error);
  }
  oncomplete(msg) {
    this.onComplete$.next(msg);
  }
 

  getAllData(targetStoreName: string) {
    return this._dbPromise.then((db: IDBPDatabase<unknown>) => {
      const tx = db.transaction(targetStoreName, 'readonly');
      const store = tx.objectStore(targetStoreName);
      return store.getAll();
    });
  }
  //#region OBSERVABLES
  dataChanged$(): Observable<IOrder[]> {
    return this.dataChange$.asObservable();
  }
  error$(): Observable<any> {
    return this.onError$.asObservable();
  }
  complete$(): Observable<any> {
    return this.onComplete$.asObservable();
  }
  //#endregion 
}
