import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subscription, ReplaySubject, Subject, of } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { IndexedDbService, IOrder } from './indexed-db.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../alert/alert.component';
const ordersStoreName = 'OrdersStore';

@Injectable({
  providedIn: 'root'
})
export class OrderService implements OnDestroy {
  public nextOrderIndex: number;
  subscription = new Subscription();
  public get List$(): Observable<IOrder[]> {
    return this.list$;
  }

  private snack$ = new Subject<any>();
  private list$ = this.indexedDB.dataChanged$();
  private error$ = this.indexedDB.error$();
  private complete$ = this.indexedDB.complete$();
  constructor(private indexedDB: IndexedDbService, private _snackBar: MatSnackBar) {
    // this.indexedDB.addItems(ordersStoreName, { name: 'yonatan', orderid: 1 });
    // this.indexedDB.addItems(ordersStoreName, { name: 'eliran', orderid: 2 });
    // this.indexedDB.addItems(ordersStoreName, { name: 'shlomo', orderid: 3 });
    this.snack$.pipe(map(s => of(this.openSnackBar(s))
    )).subscribe();
    this.indexedDB.init([ordersStoreName]);

    const sbChange = this.indexedDB.dataChanged$().pipe(map(list => {
      if (this.nextOrderIndex) {
        this.snack$.next({ message: 'new order', action: 'close', type: 'success', duration: 3000 });
      }
      this.nextOrderIndex = list[list.length - 1].orderid + 1;
    })).subscribe();

    const sbError = this.error$.pipe(map(error =>
      this.snack$.next({ message: error.message, action: 'close', type: 'error', duration: 3000 }))).subscribe();

    const sbComplete = this.complete$.pipe(map(good => {
      console.log('complete');

      this.snack$.next({ message: good, action: 'close', type: 'complete', duration: 3000 });
    })).subscribe();
    this.subscription.add(sbChange);
    this.subscription.add(sbError);
    this.subscription.add(sbComplete);
  }
  addItem(targetStoreObject: string, item: any) {
    this.indexedDB.addItem(targetStoreObject, item);
  }
  deleteItem(targetStoreObject: string, item: any) {
    this.indexedDB.deleteItem(targetStoreObject, item);
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openSnackBar(data: { message: string, action: string, type: 'error' | 'success' | 'complete', duration: number }) {

    const { message, action, duration, type } = data;
    console.log('open', type);
    const sb = this._snackBar.open(message, action, {
      duration,
      panelClass: type
    });
  }
}
