import { Component, OnInit } from '@angular/core';
import { OrderService } from '../service/order.service';
import { Observable } from 'rxjs';
import { IOrder } from '../service/indexed-db.service';
import { FormBuilder } from '@angular/forms';
const ordersStoreName = 'OrdersStore';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
})
export class CatalogComponent implements OnInit {
  public get list$(): Observable<IOrder[]> {
    return this.orderService.List$;
  }
  rForm = this.fb.group({
    orderid: [],
    name: [],
  });
  title = {
    title: 'Catalog'
  };
  constructor(private orderService: OrderService, private fb: FormBuilder) {

  }

  ngOnInit() {


  }
  onSubmit(form: IOrder) {
    this.orderService.addItem(ordersStoreName, form);
    console.log
      (form);
  }
  delete(i: IOrder) {
    this.orderService.deleteItem(ordersStoreName, i);
  }
}
